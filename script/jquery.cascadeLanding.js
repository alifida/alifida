
/*****************************************************************/
 var openEnable=true;
(function($)
{
    /**************************************************************/

    var CascadeLanding=function(cascade)
    {
        /***********************************************************/

        var $this=this;

        var cascade=$(cascade);
        var cascadeWindow=cascade.find('.cascade-window');
        var cascadeElement=cascade.find('.cascade-menu li');
        var cascadeNavigation=cascade.find('.cascade-navigation');

        var menuElement=cascade.find('.header_menu li');

        var scrollbar='';
        var selectedElement='';

       

        /***********************************************************/

        this.load=function()
        {
            
            /********************************************************/

            cascadeElement.bind('click',function(event)
            {
                event.preventDefault();
                $this.openElement(this);
               
            });
            
            
/*
            menuElement.bind('click',function(event)
            {
                event.preventDefault();
                
                
                var $menu_item=$(this);
                var $menu_item_id=$(this).attr('id');
                var $menu_item_id_array = $menu_item_id.split('_');
               
                var $cas_element= $("#"+$menu_item_id_array[2]);
                //console.log($("#about\\.html").html());
                $this.openElement($cas_element);
                
                //$menu_item.bind('click',function(event){
                //    event.preventDefault();
                   
               // });
               
            });
*/


            menuElement.each(function(){
                var $menu_item=$(this);
                var $menu_item_id=$(this).attr('id');
                var $menu_item_id_array = $menu_item_id.split('_');
                var $cas_element= $("#"+$menu_item_id_array[2]);
                
                
                $menu_item.bind('click',function(event){
                    event.preventDefault();
                    $this.openElementByMenu($cas_element);
                });
            });


            
            
           
               
            /********************************************************/

            cascadeWindow.find('.cascade-window-close-bar a').click(function(event)
            {
                
                event.preventDefault();
                $this.closeElement();
            });
            $this.closeElement();
        /********************************************************/


            
        };


        /***********************************************************/

        this.openElement=function(element)
        {
            //            alert(openEnable);
            if(!openEnable) return;

            openEnable=false;

            if($('.cascade-window-content').html()!='')
            {
                $this.closeElement();
                return;
            }

            selectedElement=$(element);

            var i=0;
            
            



            var selectedPage=selectedElement.attr('id')+".html";
            var urlParam=selectedPage ;
//            update_url(urlParam);
            var selectedClass=selectedElement.attr('class');

            selectedElement.css('z-index',2);

            cascadeElement.animate({
                left:0
            },500,'easeOutExpo',function(data)

            {
                    i++;
                    if(i==cascadeElement.length)
                    {
                        cascadeWindow.css('opacity','1');
                        cascadeWindow.css('display','block');

                        cascadeWindow.attr('class','cascade-window '+selectedClass);
                        cascadeWindow.animate({
                            width:'640px'
                        },500,'easeOutBounce',function()

                        {
                                $this.showPreloader(true);

                                $.post('page/'+selectedPage,{},function(page)
                                {
                                    $('.cascade-window-content').html(page);
                                    scrollbar=$('.cascade-window-content').jScrollPane({
                                        maintainPosition: false
                                    }).data('jsp');

                                    $this.showPreloader(false);
                                    $this.showNavigation(true);

                                    $.getScript('script/page.js');

                                    openEnable=true;
                                },
                                'html');
                            });
                    }
                    $('.scroll_left').css('display','none');
                    $('.scroll_right').css('display','none');
                });

               

        }

        /***********************************************************/
        /***********************************************************/
       this.openElementByMenu=function(element)
        {
        	
           
            if(!openEnable) return;

            openEnable=false;
            

            $this.closeElement({
                'onComplete':function()
                {
                    $this.openElement(element);
                }
            });

           
           
        }
        
        /***********************************************************/

        this.closeElement=function(data)
        {
            openEnable=false;

            if(scrollbar!='')
            {
                scrollbar.destroy();
                scrollbar='';
            }

            $(':input,a').qtip('destroy');
            $('.cascade-window-content').html('');

            $this.showNavigation(false);

            cascadeWindow.animate({
                width:'0px',
                opacity:0
            },500,'easeOutBounce',function()

            {
                    cascadeWindow.css('display','none');
                    $('.scroll_left').css('display','block');
                    $('.scroll_right').css('display','block');
                    $this.expandElements(data);
                });
        }

        /***********************************************************/

        this.expandElements=function(data)
        {
            
            var counter=0,done=0,left=-200;

            cascadeElement.each(function()
            {
                $(this).css('z-index',1);
                left+=200+((counter++)>0 ? 20 : 0);

                $(this).animate({
                    left:left
                },500,'easeOutExpo',function()

                {
                        done++;
                        if(done==cascadeElement.length)
                        {
                            openEnable=true;
                            if(typeof(data)!='undefined')
                            {
                                if(typeof(data.onComplete)!='undefined') data.onComplete.apply();
                            }
                        }
                    });
            });
        }

        /***********************************************************/

        this.createNavigation=function()
        {
            cascadeNavigation.each(function()
            {
                $(this).bind('click',function(event)
                {
                    event.preventDefault();

                    if($(this).hasClass('cascade-navigation-prev'))
                    {
                        $this.closeElement({
                            'onComplete':function()

                            {
                                var prev=selectedElement.prev();
                                if(prev.length==0) prev=cascade.find('.cascade-menu li:last');

                                $this.openElement(prev);
                            }
                        });
                    }
                    else
                    {
                        $this.closeElement({
                            'onComplete':function()

                            {
                                var next=selectedElement.next();
                                if(next.length==0) next=cascade.find('.cascade-menu li:first');

                                $this.openElement(next);
                            }
                        });
                    }
                });
            });

        
         
        }

        /***********************************************************/

        this.showNavigation=function(show)
        {
            if(cascadeElement.length>1)
                cascadeNavigation.css('display',show ? 'block' : 'none');
        }

        /***********************************************************/

        this.showPreloader=function(show)
        {
            if(show) cascadeWindow.addClass('cascade-window-prealoder');
            else cascadeWindow.removeClass('cascade-window-prealoder');
        }

    /***********************************************************/
    };

    /**************************************************************/

    $.fn.casadeLanding=function()
    {
        /***********************************************************/

        var casadeLanding=new CascadeLanding(this);

        casadeLanding.load();
        casadeLanding.createNavigation();

//        var current_url_param=get_url_param();
//        alert(current_url_param);
//        if(current_url_param != undefined && current_url_param != ""){
            //                $this.openElementByMenu($("#"+current_url_param));

//            document.getElementById('about').click();
//            document.getElementById('about').click();
        //                            $('#about').trigger('click');

//            openEnable=true;
//            casadeLanding.openElementByMenu($("#"+current_url_param));
//        }

   

    /***********************************************************/
    };

/**************************************************************/





 

})(jQuery);



//function getUsername(url){
//    var position = getDomain(url).length + 1;
//    return url.slice(position);
//}
//
//function update_url(param){
//    parent.location.hash = param;
//}
//function get_url_param(){
//    var myString = parent.location.hash ;
//    if( myString.charAt( 0 ) === '?' )
//        myString = myString.slice( 1 );
//    return myString;
//}
//
//function on_body_load(){
//
//}

