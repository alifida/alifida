<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" lang="en-US">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" lang="en-US">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html lang="en-US">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width" />
<title>Ali Fida | Java Developer Pakistan</title>


<!-- Ali Fida Script Starts  -->


<link rel="stylesheet" type="text/css" href="style/nivo-slider.css" />
<link rel="stylesheet" type="text/css" href="style/jquery.qtip.css" />
<link rel="stylesheet" type="text/css" href="style/jquery.captify.css" />
<link rel="stylesheet" type="text/css"
	href="style/jquery.jScrollPane.css" />
<link rel="stylesheet" type="text/css"
	href="style/fancybox/jquery.fancybox.css" />
<link rel="stylesheet" type="text/css" href="style/base.css" />
<link rel="stylesheet" type="text/css" href="style/page.css" />

<script type="text/javascript" src="script/jquery.min.js"></script>
<script type="text/javascript" src="script/jquery.easing.js"></script>
<script type="text/javascript" src="script/jquery.captify.js"></script>
<script type="text/javascript" src="script/jquery.blockUI.js"></script>
<script type="text/javascript" src="script/jquery.bxSlider.js"></script>
<script type="text/javascript" src="script/jquery.qtip.min.js"></script>
<script type="text/javascript" src="script/jquery.fancybox.js"></script>
<script type="text/javascript" src="script/jquery.mousewheel.js"></script>
<script type="text/javascript" src="script/jquery.jScrollPane.js"></script>
<script type="text/javascript" src="script/jquery.nivo.slider.js"></script>

<script type="text/javascript" src="script/script.js"></script>
<script type="text/javascript" src="script/jquery.cascadeLanding.js"></script>
<script type="text/javascript" src="script/main.js"></script>
<script type="text/javascript" src="plugin/contact-form/contact-form.js"></script>
<!-- 
        <script type="text/javascript" src="pitch/pitch.js"></script>
        <script type="text/javascript" src="pitch/pitch.min.js"></script>
 -->

<script src="https://apis.google.com/js/platform.js" async defer></script>

<!-- Div Scroller Starts-->

<script type="text/javascript">
            var timer1;
            function scrollDiv(divId, depl) {
                var scroll_container = document.getElementById(divId);
                scroll_container.scrollLeft -= depl;
                timer1 = setTimeout('scrollDiv("'+divId+'", '+depl+')', 10);
            }


            $(document).ready(function() {
               // on_body_load()
            });



        </script>




<!-- Div Scroller Ends-->








<!-- Ali Fida Script Ends-->

<link rel="stylesheet" type="text/css" media="all"
	href="style/style.css" />
<!--[if lt IE 9]>
        <script src="js/html5.js" type="text/javascript"></script>
        <![endif]-->
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"http:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/localhost\/alifida.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.3.1"}};
			!function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley, img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<meta name="generator" content="WordPress 4.3.1" />
<style type="text/css">
.recentcomments a {
	display: inline !important;
	padding: 0 !important;
	margin: 0 !important;
}
</style>
<script type="text/javascript" language="javascript">

        </script>
		
		
		<script data-ad-client="ca-pub-6612616707018748" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>

<body class="home blog single-author two-column right-sidebar">
	<!-- Header -->
	<div class="header main box-center">

		<div class="layout-50 clear-fix overflow-fix">

			<div class="layout-50-left">

				<a href="http://alifida.com/" title="Ali Fida" rel="home"><h1>Ali
						Fida</h1></a>
				<h5>Java Developer Pakistan</h5>
			</div>

			<div class="layout-50-right" style="text-align: right;    height: 110px; margin-top:20px">
			
			
		<div class="g-page" data-width="273" data-href="//plus.google.com/u/0/100460714061081541736" data-theme="dark" data-layout="landscape" data-rel="publisher"></div>
			<ul class="no-list header-menu" style="margin-top:5px">
					<li class="header-menu-phone"></li>
					<li class="header-menu-mail">alifida86@gmail.com</li>
				</ul>
		
			
			
				
			</div>

		</div>

	</div>
	<!-- /Header -->
	<!-- Content -->
	<div class="content main box-center">


		<div class="cascade">


			<ul class="header_menu">
				<li id="header_menu_about" class="menu_item">About</li>
				<li id="header_menu_education" class="menu_item">Education</li>
				<li id="header_menu_experience" class="menu_item">Experience</li>
				<li id="header_menu_skills" class="menu_item">Skills</li>
				<li id="header_menu_contact" class="menu_item">Contact</li>
			</ul>

			<div id="ul_container">
				<!-- Box menu -->
				<ul class="cascade-menu overflow-fix">

					<li class="blue" id="about">
						<h3>About</h3> <span>about Me</span>
					</li>

					<li class="yellow" id="education">
						<h3>Education</h3> <span>my Education</span>
					</li>
					<li class="green" id="experience">
						<h3>Experience</h3> <span>my experience</span>
					</li>
					<li class="orange" id="skills">
						<h3>Skills</h3> <span>my skills</span>
					</li>
					<li class="blue" id="contact">
						<h3>Contact</h3> <span>Contact Me</span>
					</li>
				</ul>
				<!-- /Box menu -->
			</div>


			<!-- Window -->
			<div class="cascade-window">

				<!-- Close bar -->
				<div class="cascade-window-close-bar">
					<a href="#"></a>
				</div>
				<!-- /Close bar -->

				<!-- Page content -->
				<div class="cascade-window-content"></div>
				<!-- /Page content -->

				<!-- Footer -->
				<div class="cascade-window-footer"></div>
				<!-- /Footer -->

			</div>
			<!-- /Window -->

			<!-- Navigation -->
			<a href="#" class="cascade-navigation cascade-navigation-prev"></a> <a
				href="#" class="cascade-navigation cascade-navigation-next"></a> <a
				href="javascript:void(0)" class="scroll_left"
				onmouseover="scrollDiv('ul_container', 10)"
				onmouseout="clearTimeout(timer1)"></a> <a href="javascript:void(0)"
				class="scroll_right" onmouseover="scrollDiv('ul_container', -10)"
				onmouseout="clearTimeout(timer1)"></a>
			<!-- /Navigation -->

		</div>
		<!-- /Cascade -->

	</div>
	<!-- /Content -->



	<!-- #main -->

	<!-- Footer -->
	<div class="footer">

		<hr class="footer-line" />

		<div class="main box-center layout-7030 clear-fix overflow-fix">

			<!-- Latest tweets -->
			<div class="layout-7030-left latest-tweets">
				<div id="latest-tweets"></div>
			</div>
			<!-- /Latest tweets -->

			<!-- Social icons -->
			<div class="layout-7030-right">

				<ul class="no-list social-list">
					<li>Connect</li>
					<li><a href="#" class="social-rss"></a></li>
					<li><a href="#" class="social-facebook"></a></li>
					<li><a href="#" class="social-twitter"></a></li>
					<li><a href="#" class="social-google"></a></li>
					<li><a href="#" class="social-skype"></a></li>
				</ul>

			</div>
			<!-- /Social icons -->

		</div>

	</div>
	<!-- /Footer -->
 
</body>
</html>